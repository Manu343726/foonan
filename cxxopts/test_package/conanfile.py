from conans import ConanFile, CMake
from conans.tools import download
import os

class CxxOptsTest(ConanFile):
    generators = 'cmake'

    def build(self):
        download('https://raw.githubusercontent.com/jarro2783/cxxopts/v{version}/src/example.cpp' \
            .format(version=self.requires['cxxopts'].conan_reference.version), os.path.join(self.source_folder, 'example.cpp'))

        cmake = CMake(self)
        cmake.configure(defs={
            'CXXOPTS_BUILD_EXAMPLES': True,
            'CMAKE_VERBOSE_MAKEFILE': True
        })
        cmake.build()
        os.remove(os.path.join(self.source_folder, 'example.cpp'))

    def test(self):
        self.run('./bin/example --help')
