from conans import ConanFile, tools
import os

DEFAULT_CXXOPTS_VERSION = '2.1.0'

class CxxOptsConan(ConanFile):
    name = 'cxxopts'
    version = os.environ.get("CONAN_VERSION_OVERRIDE", DEFAULT_CXXOPTS_VERSION)
    generators = "cmake", "cmake_find_package"
    url = 'https://gitlab.com/Manu343726/conan-cxxopts'
    license = 'MIT'

    def source(self):
        git = tools.Git(folder='cxxopts')
        git.clone('https://github.com/jarro2783/cxxopts.git', 'v' + self.version)

    def package(self):
        self.copy('cxxopts/include/cxxopts.hpp', dst="include", keep_path=False)

    def package_id(self):
        self.info.header_only()
