from conans import ConanFile, CMake
import os

class ConanCppAstTest(ConanFile):
    settings = "os", "compiler", "build_type", "arch"
    generators = 'cmake'

    def build(self):
        cmake = CMake(self)
        cmake.configure(defs={
            "CMAKE_VERBOSE_MAKEFILE": True,
            "CMAKE_EXPORT_COMPILE_COMMANDS": True
        })
        cmake.build()

    def test(self):
        self.run(os.path.join('bin', 'ast_printer') + ' example.hpp')
