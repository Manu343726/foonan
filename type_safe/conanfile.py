import conans
from conans import ConanFile, python_requires
from conans.tools import download, untargz
import os

class TypeSafe(ConanFile):
    name = 'type_safe'
    url  = 'https://github.com/foonathan/type_safe'
    description = 'Zero overhead utilities for preventing bugs at compile time'
    license = 'MIT'
    version = '0.3'
    requires = (
        'debug_assert/1.3.3-1@Manu343726/testing',
        'conan_recipe_tools/0.1@Manu343726/testing'
    )
    generators = 'cmake', 'cmake_find_package'

    def source(self):
        from conans_importer import import_conans
        import_conans(conans)

        from source import github_repo
        github_repo(self, username='Manu343726')

    def package_id(self):
        self.info.header_only()

    def package(self):
        from package import package_headers
        package_headers(self)
