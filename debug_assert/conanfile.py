from conans import python_requires
import os

common = python_requires('conan_common_recipes/0.0.8@Manu343726/testing')

class DebugAssert(common.HeaderOnlyPackage):
    name = 'debug_assert'
    url  = 'https://gitlab.com/Manu343726/foonan'
    description = 'Simple, flexible and modular assertion macro'
    license = 'MIT'
    version = '1.3.3-1'
    generators = 'cmake', 'cmake_find_package'
    settings = 'os', 'compiler', 'build_type', 'arch'

    scm = {
      'type': 'git',
      'url': 'https://github.com/foonathan/debug_assert',
      'revision': '130adcbb393befa29d70036d93905f7bd94aa110',
      'subfolder': 'debug_assert'
    }

    def package(self):
        self.copy('debug_assert.hpp', src='debug_assert', dst='include')
